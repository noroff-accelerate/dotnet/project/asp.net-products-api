# Product API Project
## Overview
This ASP.NET Core Web API project provides a simple in-memory CRUD operation for a product catalog. It allows adding, retrieving, updating, deleting, and searching for products.

## Prerequisites
- .NET 6.0 SDK
- Visual Studio with ASP.NET Core development tools
## Setup
1. Clone or download the repository to your local machine.
2. Open the solution file in Visual Studio.
3. Build the solution to restore NuGet packages.
## Running the Application
1. In Visual Studio, press F5 or click on the "IIS Express" button to run the application.
2. The application will start and open in your default web browser.
## API Endpoints
The application exposes the following RESTful endpoints:

- GET /api/products: Retrieves all products.
- GET /api/products/{id}: Retrieves a product by its ID.
- POST /api/products: Adds a new product.
- PUT /api/products/{id}: Updates an existing product.
- DELETE /api/products/{id}: Deletes a product by ID.
- GET /api/products/search/{keyword}: Searches for products by a keyword.
## Models
### Product
- Id (int): The unique identifier for the product.
- Name (string): The name of the product.
- Description (string): The description of the product.
- Price (decimal): The price of the product.
## Testing the API
You can test the API endpoints using Postman.

1. Run the application.
2. Use the tool to send HTTP requests to the endpoints.
3. Verify the responses from the API.
## Contributing
Contributions to this project are welcome. Please open an issue or submit a pull request with your suggested changes.

## License
This project is licensed under the MIT License - see the LICENSE file for details.
