﻿namespace Noroff.Samples.ASPNET.ProductsAPI.Data
{
    public static class DataStore
    {
        public static List<Product> Products = new List<Product>
        {
            new Product { Id = 1, Name = "Laptop", Description = "A portable computer", Price = 1000.00M },
            new Product { Id = 2, Name = "Smartphone", Description = "A handheld device", Price = 500.00M },
            new Product { Id = 3, Name = "Headphones", Description = "Audio device", Price = 150.00M }
        };
    }

}
