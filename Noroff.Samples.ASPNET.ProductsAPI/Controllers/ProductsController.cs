﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Noroff.Samples.ASPNET.ProductsAPI.Data;

namespace Noroff.Samples.ASPNET.ProductsAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        [HttpGet]
        public IActionResult GetAllProducts()
        {
            return Ok(DataStore.Products);
        }

        [HttpGet("{id}")]
        public IActionResult GetProductById(int id)
        {
            var product = DataStore.Products.FirstOrDefault(p => p.Id == id);
            if (product == null)
            {
                return NotFound();
            }
            return Ok(product);
        }

        [HttpPost]
        public IActionResult AddProduct([FromBody] Product newProduct)
        {
            var existingProduct = DataStore.Products.Any(p => p.Id == newProduct.Id);
            if (existingProduct)
            {
                return BadRequest("Product with the same ID already exists.");
            }

            DataStore.Products.Add(newProduct);
            return CreatedAtAction(nameof(GetProductById), new { id = newProduct.Id }, newProduct);
        }

        [HttpPut("{id}")]
        public IActionResult UpdateProduct(int id, [FromBody] Product updatedProduct)
        {
            var product = DataStore.Products.FirstOrDefault(p => p.Id == id);
            if (product == null)
            {
                return NotFound();
            }

            product.Name = updatedProduct.Name;
            product.Description = updatedProduct.Description;
            product.Price = updatedProduct.Price;

            return Ok(product);
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteProduct(int id)
        {
            var product = DataStore.Products.FirstOrDefault(p => p.Id == id);
            if (product == null)
            {
                return NotFound();
            }

            DataStore.Products.Remove(product);
            return NoContent();
        }

        [HttpGet("search/{keyword}")]
        public IActionResult SearchProducts(string keyword)
        {
            var results = DataStore.Products.Where(p => p.Name.Contains(keyword, StringComparison.OrdinalIgnoreCase) || p.Description.Contains(keyword, StringComparison.OrdinalIgnoreCase)).ToList();
            if (!results.Any())
            {
                return NotFound();
            }

            return Ok(results);
        }
    }
}
